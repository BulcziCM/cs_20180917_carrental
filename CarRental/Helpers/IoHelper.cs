﻿using System;

namespace CarRental.Helpers
{
    internal static class IoHelper
    {
        public static string GetStringFromUser(string message)
        {
            Console.Write($"{message}: ");
            return Console.ReadLine();
        }

        public static int GetIntFromUser(string message)
        {
            Console.Write($"{message}: ");

            int number;
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Provided value is not a number!");
            }

            return number;
        }
    }
}