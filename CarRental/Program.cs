﻿using CarRental.Business.Models;
using CarRental.Business.Services;
using CarRental.Helpers;
using System;
using System.Collections.Generic;

namespace CarRental
{
    internal class Program
    {
        private CarsService _carsService = new CarsService();
        private CustomersService _customerService = new CustomersService();
        //private RentalService _rentalService = new RentalService();

        private Menu _menu = new Menu();

        private static void Main()
        {
            new Program().Run();
        }

        private void Run()
        {
            _menu.AddCommand("AddCar", CreateCars);
            _menu.AddCommand("AddCustomer", CreateCustomers);
            _menu.AddCommand("RentCar", RentCar);
            _menu.AddCommand("PrintFreeCarsCount", PrintFreeCarsCount);
            _menu.AddCommand("GetRentedCarsNames", GetRentedCarsNames);
            _menu.AddCommand("UpdateCustomer", UpdateCustomer);

            while (true)
            {
                _menu.PrintCommands();
                var command = IoHelper.GetStringFromUser("Enter command");
                _menu.RunCommand(command);
            }
        }

        private void UpdateCustomer()
        {
            Console.Write("Type user pesel: ");
            var pesel = Console.ReadLine();

            var customer = _customerService.GetCustomer(pesel);

            Console.Write("Enter new name: ");
            customer.Name = Console.ReadLine();
            Console.Write("Enter new surname: ");
            customer.Surname = Console.ReadLine();

            _customerService.Update(customer);
        }

        private void GetRentedCarsNames()
        {
            Console.WriteLine("Rented cars:");
            foreach(var carName in _carsService.GetRentedCarsNames())
            {
                Console.WriteLine($"{carName}");
            }
        }

        private void PrintFreeCarsCount()
        {
            Console.WriteLine($"Free cars: {_carsService.GetFreeCarsCount()}");
        }

        private void RentCar()
        {
            //List<CarBl> carsList = _carsService.GetAll();
            //List<CustomerBl> customersList = _customerService.GetAll();

            //foreach (var element in customersList)
            //{
            //    Console.WriteLine($"{element.Name} {element.Surname} {element.Pesel}");
            //}

            //var rentingCustomerPesel = IoHelper.GetStringFromUser("Provide customer's pesel");

            //foreach (var element in carsList)
            //{
            //    Console.WriteLine($"{element.Brand} {element.Model} {element.LicensePlace}");
            //}

            //var rentedCarLicensePlate = IoHelper.GetStringFromUser("Provide car's license plate");

            //_rentalService.Rent(rentingCustomerPesel, rentedCarLicensePlate);
        }

        private void CreateCars()
        {
            var newCar = new CarBl
            {
                Brand = IoHelper.GetStringFromUser("Brand"),
                Model = IoHelper.GetStringFromUser("Model"),
                LicensePlace = IoHelper.GetStringFromUser("LicensePlate"),
                PricePerDay = IoHelper.GetIntFromUser("Price per day (PLN)"),
                IsRented = false
            };

            try
            {
                _carsService.AddCar(newCar);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void CreateCustomers()
        {
            var newCustomer = new CustomerBl
            {
                Name = IoHelper.GetStringFromUser("Name"),
                Surname = IoHelper.GetStringFromUser("Surname"),
                Pesel = IoHelper.GetStringFromUser("Pesel")
            };

            _customerService.AddCustomer(newCustomer);
        }
    }
}