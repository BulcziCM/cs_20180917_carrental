﻿using System;
using System.Collections.Generic;
using CarRental.Business.Models;
using CarRental.Business.Services;
using CarRental.Data.Models;
using CarRental.Data.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarRental.Business.Tests
{
    [TestClass]
    public class CarsServiceTests
    {
        [TestMethod]
        public void GetFreeCarsCount_ValidListOfCars_CorrectCountResult()
        {
            //Arrange
            var carsList = new List<Car>
            {
                new Car
                {
                    IsRented = false
                },
                new Car
                {
                    IsRented = true
                }
            };

            var carsServiceMock = new Mock<ICarsRepository>();
            carsServiceMock.Setup(x => x.GetAll()).Returns(carsList);

            var carsService = new CarsService(carsServiceMock.Object, null);

            //Act
            var result = carsService.GetFreeCarsCount();

            //Assert
            Assert.AreEqual(1, result);
        }

        //Test jest nieprawidlowy, poneiwaz testuje wiecej niz jeden przypadek!!!
        //[TestMethod]
        //public void GetCar_ValidAndInvalidLicense_ValidObjectOrNull()
        //{
        //    //Arrange
        //    const string validLicense   = "GD11111";
        //    const string invalidLicense = "GA99999";

        //    var validCar = new Car
        //    {
        //        Id = 1
        //    };
        //    Car invalidCar = null;

        //    var validCarBl = new CarBl
        //    {
        //        Id = 1
        //    };
        //    CarBl invalidCarBl = null;

        //    var carsRepositoryMock = new Mock<ICarsRepository>();
        //    carsRepositoryMock.Setup(x => x.Get(validLicense)).Returns(validCar);
        //    carsRepositoryMock.Setup(x => x.Get(invalidLicense)).Returns(invalidCar);

        //    var dataObjectMapperMock = new Mock<IDataObjectsMapper>();
        //    dataObjectMapperMock.Setup(x => x.MapCarToCarBl(validCar)).Returns(validCarBl);
        //    dataObjectMapperMock.Setup(x => x.MapCarToCarBl(invalidCar)).Returns(invalidCarBl);

        //    var carsService = new CarsService(
        //        carsRepositoryMock.Object,
        //        dataObjectMapperMock.Object);

        //    //Act
        //    var validResult   = carsService.GetCar(validLicense);
        //    var invalidResult = carsService.GetCar(invalidLicense);

        //    //Assert
        //    Assert.AreEqual(validCarBl, validResult);
        //    Assert.AreEqual(invalidCarBl, invalidResult);
        //}

        [TestMethod]
        public void GetCar_ValidLicense_ValidObjectl()
        {
            //Arrange
            var validCar = new Car
            {
                Id = 1
            };

            var validCarBl = new CarBl
            {
                Id = 1
            };

            var carsRepositoryMock = new Mock<ICarsRepository>();
            carsRepositoryMock.Setup(x => x.Get(It.IsAny<string>())).Returns(validCar);

            var dataObjectMapperMock = new Mock<IDataObjectsMapper>();
            dataObjectMapperMock.Setup(x => x.MapCarToCarBl(validCar)).Returns(validCarBl);

            var carsService = new CarsService(
                carsRepositoryMock.Object,
                dataObjectMapperMock.Object);

            //Act
            var validResult = carsService.GetCar("ValidLicense");

            //Assert
            Assert.AreEqual(validCarBl, validResult);
        }

        [TestMethod]
        public void GetCar_InvalidLicense_Null()
        {
            //Arrange
            const string invalidLicense = "GA99999";

            Car invalidCar = null;
            CarBl invalidCarBl = null;

            var carsRepositoryMock = new Mock<ICarsRepository>();
            carsRepositoryMock.Setup(x => x.Get(invalidLicense)).Returns(invalidCar);

            var dataObjectMapperMock = new Mock<IDataObjectsMapper>();
            dataObjectMapperMock.Setup(x => x.MapCarToCarBl(invalidCar)).Returns(invalidCarBl);

            var carsService = new CarsService(
                carsRepositoryMock.Object,
                dataObjectMapperMock.Object);

            //Act
            var invalidResult = carsService.GetCar(invalidLicense);

            //Assert
            Assert.AreEqual(invalidCarBl, invalidResult);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void AddCar_RepositoryThrowsException_ThrowsException()
        {
            //Arrange
            var car = new Car();
            var carBl = new CarBl();

            var carsRepositoryMock = new Mock<ICarsRepository>();
            carsRepositoryMock.Setup(x => x.Add(car)).Throws(new Exception());

            var dataObjectMapperMock = new Mock<IDataObjectsMapper>();
            dataObjectMapperMock.Setup(x => x.MapCarBlToCar(carBl)).Returns(car);

            var carsService = new CarsService(
                carsRepositoryMock.Object,
                dataObjectMapperMock.Object);

            //Act
            carsService.AddCar(carBl);
        }
    }
}