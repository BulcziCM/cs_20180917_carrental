﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarRental.Data.Models;

namespace CarRental.Data.Repositories
{
    public interface ICarsRepository
    {
        List<Car> GetAll();
        Car Get(string licensePlate);
        void Add(Car carToAdd);
    }

    public class CarsRepository : ICarsRepository
    {
        public List<Car> GetAll()
        {
            using (var dbContext = new CarRentalDbContext())
            {
                return dbContext.CarsDbSet.ToList();
            }
        }

        public Car Get(string licensePlate)
        {
            using (var dbContext = new CarRentalDbContext())
            {
                return dbContext.CarsDbSet
                    .FirstOrDefault(x => x.LicensePlates == licensePlate);
            }
        }

        public void Add(Car carToAdd)
        {
            using (var dbContext = new CarRentalDbContext())
            {
                if (dbContext.CarsDbSet.Any(c => c.LicensePlates == carToAdd.LicensePlates))
                {
                    throw new Exception($"Car with license plate {carToAdd.LicensePlates} already exists");
                }

                dbContext.CarsDbSet.Add(carToAdd);
                dbContext.SaveChanges();
            }
        }
    }
}