﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CarRental.Data.Models;

namespace CarRental.Data.Repositories
{
    public class CustomersRepository
    {
        public List<Customer> GetAll()
        {
            using (var dbContext = new CarRentalDbContext())
            {
                return dbContext.CustomersDbSet.ToList();
            }
        }

        public Customer Get(string pesel)
        {
            using (var dbContext = new CarRentalDbContext())
            {
                return dbContext.CustomersDbSet
                    .FirstOrDefault(x => x.Pesel == pesel);
            }
        }

        public void Add(Customer customerToAdd)
        {
            using (var dbContext = new CarRentalDbContext())
            {
                dbContext.CustomersDbSet.Add(customerToAdd);
                dbContext.SaveChanges();
            }
        }

        public void Update(Customer customerToUpdate)
        {
            using (var dbContex = new CarRentalDbContext())
            {
                if (!dbContex.CustomersDbSet.Local.Contains(customerToUpdate))
                {
                    dbContex.CustomersDbSet.Attach(customerToUpdate);
                    dbContex.Entry(customerToUpdate).State = EntityState.Modified;
                }

                dbContex.SaveChanges();
            }
        }
    }
}