﻿using System.Configuration;
using System.Data.Entity;
using CarRental.Data.Models;

namespace CarRental.Data
{
    internal class CarRentalDbContext : DbContext
    {
        public CarRentalDbContext() : base(GetConnectionString())
        {
        }

        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<Car> CarsDbSet { get; set; }
        public DbSet<Customer> CustomersDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["CarRentalDbConnectionString"].ConnectionString;
        }
    }
}