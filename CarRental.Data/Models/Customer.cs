﻿using System.Collections.Generic;

namespace CarRental.Data.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Pesel { get; set; }
        public List<Car> RentedCars { get; set; }
    }
}