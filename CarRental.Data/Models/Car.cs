﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Data.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string LicensePlates { get; set; }
        public bool   IsRented { get; set; }
        public double PricePerDay { get; set; }
        public List<Customer> RentingCustomers { get; set; }
    }
}