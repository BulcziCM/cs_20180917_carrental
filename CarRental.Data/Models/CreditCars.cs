﻿namespace CarRental.Data.Models
{
    public class CreditCard
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public Customer Customer { get; set; }
    }
}