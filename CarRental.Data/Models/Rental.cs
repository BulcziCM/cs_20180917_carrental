﻿using System;

namespace CarRental.Data.Models
{
    public class Rental
    {
        public Car Car { get; set; }
        public Customer Customer { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}