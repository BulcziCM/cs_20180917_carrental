﻿using System;

namespace CarRental.Business.Models
{
    public class RentalBl
    {
        public CarBl Car;
        public CustomerBl Customer;
        public DateTime StartDate;
        public DateTime EndDate;
    }
}