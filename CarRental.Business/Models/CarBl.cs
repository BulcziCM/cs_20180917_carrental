﻿namespace CarRental.Business.Models
{
    public class CarBl
    {
        public int Id;
        public string Brand;
        public string Model;
        public string LicensePlace;
        public bool   IsRented;
        public double PricePerDay;
    }
}