﻿using CarRental.Business.Models;
using CarRental.Data.Repositories;
using System;
using System.Collections.Generic;

namespace CarRental.Business.Services
{
    public class CustomersService
    {
        private CustomersRepository _customersRepository = new CustomersRepository();
        private IDataObjectsMapper _dataObjectsMapper = new DataObjectsMapper();

        public List<CustomerBl> GetAll()
        {
            var customers = _customersRepository.GetAll();
            var customersBl = new List<CustomerBl>();

            foreach (var customer in customers)
            {
                customersBl.Add(_dataObjectsMapper.MapCustomerToCustomerBl(customer));
            }

            return customersBl;
        }

        public CustomerBl GetCustomer(string pesel)
        {
            return _dataObjectsMapper.MapCustomerToCustomerBl(_customersRepository.Get(pesel));
        }

        public void AddCustomer(CustomerBl customerBl)
        {
            _customersRepository.Add(_dataObjectsMapper.MapCustomerBlToCustomer(customerBl));
        }

        public void Update(CustomerBl customerBl)
        {
            _customersRepository.Update(_dataObjectsMapper.MapCustomerBlToCustomer(customerBl));
        }
    }
}