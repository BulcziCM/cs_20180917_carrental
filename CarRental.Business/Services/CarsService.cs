﻿using CarRental.Business.Models;
using CarRental.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("CarRental.Business.Tests")]
namespace CarRental.Business.Services
{
    public class CarsService
    {
        private ICarsRepository _carsRepositry;
        private IDataObjectsMapper _dataObjectsMapper;

        public CarsService()
        {
            _carsRepositry     = new CarsRepository();
            _dataObjectsMapper = new DataObjectsMapper();
        }

        internal CarsService(
            ICarsRepository carsRepositry,
            IDataObjectsMapper dataObjectsMapper)
        {
            _carsRepositry = carsRepositry;
            _dataObjectsMapper = dataObjectsMapper;
        }

        public List<CarBl> GetAll()
        {
            var cars = _carsRepositry.GetAll();
            var carsBl = new List<CarBl>();

            foreach(var car in cars)
            {
                carsBl.Add(_dataObjectsMapper.MapCarToCarBl(car));
            }

            return carsBl;
        }

        public CarBl GetCar(string licensePlate)
        {
            var car = _carsRepositry.Get(licensePlate);
            return _dataObjectsMapper.MapCarToCarBl(car);
        }

        public void AddCar(CarBl carBl)
        {
            try
            {
                _carsRepositry.Add(_dataObjectsMapper.MapCarBlToCar(carBl));
            }
            catch(Exception e)
            {
                throw new Exception("Adding new car failed!", e);
            }
        }

        public List<string> GetRentedCarsNames()
        {
            var collection = _carsRepositry.GetAll()
                .Where(c => c.IsRented)
                .Select(c => $"{c.Brand} {c.Model}");

            return collection.ToList();
        }

        public int GetFreeCarsCount()
        {
            var list = _carsRepositry.GetAll();
            var result = list.Count(c => !c.IsRented);
            return result;
        }
    }
}