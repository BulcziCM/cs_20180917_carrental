﻿using CarRental.Business.Models;
using CarRental.Data.Models;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: InternalsVisibleTo("CarRental.Business.Tests")]
namespace CarRental.Business
{
    internal interface IDataObjectsMapper
    {
        Car MapCarBlToCar(CarBl carBl);
        CarBl MapCarToCarBl(Car car);
        Customer MapCustomerBlToCustomer(CustomerBl customerBl);
        CustomerBl MapCustomerToCustomerBl(Customer customer);
        Rental MapRentalBlToRental(RentalBl rentalBl);
        RentalBl MapRentalToRentalBl(Rental rental);
    }

    internal class DataObjectsMapper : IDataObjectsMapper
    {
        public Car MapCarBlToCar(CarBl carBl)
        {
            var car = new Car
            {
                Id = carBl.Id,
                Brand = carBl.Brand,
                Model = carBl.Model,
                LicensePlates = carBl.LicensePlace,
                IsRented = carBl.IsRented,
                PricePerDay = carBl.PricePerDay
            };

            return car;
        }

        public CarBl MapCarToCarBl(Car car)
        {
            if (car == null)
            {
                return null;
            }

            var carBl = new CarBl
            {
                Id = car.Id,
                Brand = car.Brand,
                Model = car.Model,
                LicensePlace = car.LicensePlates,
                IsRented = car.IsRented,
                PricePerDay = car.PricePerDay
            };

            return carBl;
        }

        public Customer MapCustomerBlToCustomer(CustomerBl customerBl)
        {
            var customer = new Customer
            {
                Id = customerBl.Id,
                Name = customerBl.Name,
                Surname = customerBl.Surname,
                Pesel = customerBl.Pesel
            };

            return customer;
        }

        public CustomerBl MapCustomerToCustomerBl(Customer customer)
        {
            var customerBl = new CustomerBl
            {
                Id = customer.Id,
                Name = customer.Name,
                Surname = customer.Surname,
                Pesel = customer.Pesel
            };

            return customerBl;
        }

        public Rental MapRentalBlToRental(RentalBl rentalBl)
        {
            var rental = new Rental
            {
                Car = MapCarBlToCar(rentalBl.Car),
                Customer = MapCustomerBlToCustomer(rentalBl.Customer),
                StartDate = rentalBl.StartDate,
                EndDate = rentalBl.EndDate
            };

            return rental;
        }

        public RentalBl MapRentalToRentalBl(Rental rental)
        {
            var rentalBl = new RentalBl
            {
                Car = MapCarToCarBl(rental.Car),
                Customer = MapCustomerToCustomerBl(rental.Customer),
                StartDate = rental.StartDate,
                EndDate = rental.EndDate
            };

            return rentalBl;
        }
    }
}